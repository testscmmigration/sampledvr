package com.moneygram.dvc.persistence;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.moneygram.dvc.bo.AddressBO;
import com.moneygram.dvc.persistence.BadAddressDao.BadAddressMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath*:/META-INF/spring/dvc-test-component.xml")
public class BadAddressDaoTest {

	@Autowired
	BadAddressDao dao;
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetRowMapper() {
		RowMapper<?> mapper = dao.getRowMapper();
		Assert.assertNotNull("getRowMapper returned null.", mapper);
		Assert.assertTrue("Invalid RowMapper instance", mapper instanceof BadAddressMapper);
	}
	
	@Test
	public void testGetStoredProcedureName() {
		String s = dao.getStoredProcedureName();
		Assert.assertNotNull("getStoredProcedureName returned null", s);
		Assert.assertEquals(BadAddressDao.BAD_ADD_PROC_NAME, s);
	}

	@Test 
	public void testGetProcessLogIdOutputName() {
		String s = dao.getProcessLogIdOutputName();
		Assert.assertNotNull("getProcessLogIdOutputName returned null.", s);
		Assert.assertEquals(BadAddressDao.PROCESS_LOG_ID, s);
	}

	@Test
	public void testGetBusinessAreaCodeInputName() {
		String s = dao.getBusinessAreaCodeInputName();
		Assert.assertNotNull("getBusinessAreaCodeInputName returned null.", s);
		Assert.assertEquals(BadAddressDao.BUSINESS_CODE, s);
	}

	@Test
	public void testGetCallTypeCodeInputName() {
		String s = dao.getCallTypeCodeInputName();
		Assert.assertNotNull("getCallTypeCodeInputName returned null.", s);
		Assert.assertEquals(BadAddressDao.CALL_TYPE_CODE, s);
	}

	@Test
	public void testGetCursorOutputName() {
		String s = dao.getCursorOutputName();
		Assert.assertNotNull("getCursorOutputName returned null.", s);
		Assert.assertEquals(BadAddressDao.OUTPUT_CURSOR, s);
	}

	@Test
	public void testFind() throws Exception {
		Assert.assertNotNull(dao);
		
		List<?> badAddresses = dao.find();
		Assert.assertNotNull("badAddresses is null.", badAddresses);
		Assert.assertFalse("badAddresses is empty", badAddresses.isEmpty());
		Assert.assertTrue("baddAddress has contains incorrect object type", badAddresses.get(0) instanceof AddressBO);
	}
	
	@Test 
	public void testFindNonExistingBusinessAreaCode() throws Exception {
		
		Assert.assertNotNull(dao);
		dao.setBusinessAreaCode("ZZZXYZ");
		
		List<?> badAddresses = dao.find();
		Assert.assertNotNull("badAddresses is null", badAddresses);
		Assert.assertTrue("Expecting an empty list", badAddresses.isEmpty());
	}
}
