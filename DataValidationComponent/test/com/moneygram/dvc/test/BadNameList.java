package com.moneygram.dvc.test;

import java.util.ArrayList;
import java.util.List;

import com.moneygram.dvc.bo.ConsumerNameBO;
import com.moneygram.rule.common.datalist.RuleDataListFacade;

public class BadNameList implements RuleDataListFacade {

	@Override
	public List<?> find() throws Exception {
		List<ConsumerNameBO> list = new ArrayList<ConsumerNameBO>();
		
		ConsumerNameBO badName = new ConsumerNameBO();
		badName.setFirstName("MONEYGRAM");
		badName.setLastName("MONEYGRAM");
		list.add(badName);
		
		return list;
	}

}
