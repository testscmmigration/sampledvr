package com.moneygram.dvr.service.bean;


import ilog.rules.res.model.IlrPath;
import ilog.rules.res.session.IlrPOJOSessionFactory;
import ilog.rules.res.session.IlrSessionFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.moneygram.cache.BadAddressCache;
import com.moneygram.cache.BadNameCache;
import com.moneygram.cache.BadPhoneNumbersCache;
import com.moneygram.cache.CountryCache;
import com.moneygram.cache.CountrySubDivCache;
import com.moneygram.cache.CurrencyInfoCache;
import com.moneygram.cache.ESBMetaDataCache;
import com.moneygram.cache.ESBODMDataMappingCache;
import com.moneygram.cache.EnumerationDataCache;
import com.moneygram.cache.FactoryClassDataCache;
import com.moneygram.cache.GetAllFieldsCache;
import com.moneygram.cache.StateProvinceCache;
import com.moneygram.cache.impl.BackupCacheImpl;
import com.moneygram.cache.impl.EnumerationDataCacheImpl;
import com.moneygram.common.util.ApplicationContextFactory;
import com.moneygram.common.util.ApplicationContextProvider;
import com.moneygram.constants.ReferenceConstants;
import com.moneygram.esb.clients.countryinfo.CountryInfo;
import com.moneygram.exception.RVSErrorCodes;
import com.moneygram.exception.RequestValidationServiceException;

public class RVSServiceStartupBean {
	
	private static final Logger logger = org.apache.log4j.Logger.getLogger(RVSServiceStartupBean.class.getName());
	private static final String[] CONTEXT_FILES = { "/applicationContext.xml" };
	
	@Autowired
	private CountryCache countryCache;
	@Autowired
	private CountrySubDivCache countrySubDivCache;
	@Autowired
	private StateProvinceCache stateProvinceCache;
	@Autowired
	private CurrencyInfoCache currencyInfoCache;
	@Autowired
	private GetAllFieldsCache getAllFieldsCache;
	@Autowired
	private EnumerationDataCache enumerationDataCache;
	@Autowired
	private BadPhoneNumbersCache badPhoneNumbersCache;
	@Autowired
	private BadNameCache badNameCache;
	@Autowired
	private BadAddressCache badAddressCache;
	@Autowired
	private ESBODMDataMappingCache esbOdmDataMappingCache;
	@Autowired
	private ESBMetaDataCache esbMetaDataCache;
	@Autowired
	private FactoryClassDataCache factoryClassDataCache;
	@Autowired
	private static String FNC_NM_ORCHS_RCV_VLDN;
	@Autowired
	private static String FNC_NM_ORCHS_SESSION_CMPLN;
	@Autowired
	private static String FNC_NM_ORCHS_GET_FIELDS_FOR_PRDTS;
	@Autowired
	private static String FNC_NM_ORCHS_SND_RVRSL_VLDN;
	@Autowired
	private static String FNC_NM_ORCHS_SND_RVRSL_SSN_CMPLN;
	@Autowired
	private static String FNC_NM_ORCHS_SND_VLDN;
	@Autowired
	private static String FNC_NM_ORCHS_BP_VLDN;
	@Autowired
	private static String FNC_NM_ORCHS_AMEND_VLDN;
	@Autowired
	private static String FNC_NM_DOT_ORCHS_RCV_VLDN;
	@Autowired
	private static String FNC_NM_DOT_ORCHS_SESSION_CMPLN;
	@Autowired
	private static String FNC_NM_DOT_ORCHS_GET_FIELDS_FOR_PRDTS;
	@Autowired
	private static String FNC_NM_DOT_ORCHS_SND_RVRSL_VLDN;
	@Autowired
	private static String FNC_NM_DOT_ORCHS_SND_RVRSL_SSN_CMPLN;
	@Autowired
	private static String FNC_NM_DOT_ORCHS_SND_VLDN;
	@Autowired
	private static String FNC_NM_DOT_ORCHS_BP_VLDN;
	@Autowired
	private static String FNC_NM_DOT_ORCHS_AMND_VLDN;
	/*
	@Autowired
	private static String FNC_NM_ORCHS_RCV_RVRSL;
	@Autowired
	private static String FNC_ORCHS_RCV_RVRSL_SSN_COMPLN;
	@Autowired
	private static String FNC_NM_DOT_ORCHS_RCV_RVRSL;
	@Autowired
	private static String FNC_NM_DOT_ORCHS_RCV_RVRSL_SSN_COMPLN;
	*/
	
	
	public static String getFNC_NM_DOT_ORCHS_RCV_VLDN() {
		return FNC_NM_DOT_ORCHS_RCV_VLDN;
	}

	public static void setFNC_NM_DOT_ORCHS_RCV_VLDN(String fNC_NM_DOT_ORCHS_RCV_VLDN) {
		FNC_NM_DOT_ORCHS_RCV_VLDN = fNC_NM_DOT_ORCHS_RCV_VLDN;
	}

	public static String getFNC_NM_DOT_ORCHS_SESSION_CMPLN() {
		return FNC_NM_DOT_ORCHS_SESSION_CMPLN;
	}

	public static void setFNC_NM_DOT_ORCHS_SESSION_CMPLN(
			String fNC_NM_DOT_ORCHS_SESSION_CMPLN) {
		FNC_NM_DOT_ORCHS_SESSION_CMPLN = fNC_NM_DOT_ORCHS_SESSION_CMPLN;
	}

	public static String getFNC_NM_DOT_ORCHS_GET_FIELDS_FOR_PRDTS() {
		return FNC_NM_DOT_ORCHS_GET_FIELDS_FOR_PRDTS;
	}

	public static void setFNC_NM_DOT_ORCHS_GET_FIELDS_FOR_PRDTS(
			String fNC_NM_DOT_ORCHS_GET_FIELDS_FOR_PRDTS) {
		FNC_NM_DOT_ORCHS_GET_FIELDS_FOR_PRDTS = fNC_NM_DOT_ORCHS_GET_FIELDS_FOR_PRDTS;
	}

	public static String getFNC_NM_DOT_ORCHS_SND_RVRSL_VLDN() {
		return FNC_NM_DOT_ORCHS_SND_RVRSL_VLDN;
	}

	public static void setFNC_NM_DOT_ORCHS_SND_RVRSL_VLDN(
			String fNC_NM_DOT_ORCHS_SND_RVRSL_VLDN) {
		FNC_NM_DOT_ORCHS_SND_RVRSL_VLDN = fNC_NM_DOT_ORCHS_SND_RVRSL_VLDN;
	}

	public static String getFNC_NM_DOT_ORCHS_SND_RVRSL_SSN_CMPLN() {
		return FNC_NM_DOT_ORCHS_SND_RVRSL_SSN_CMPLN;
	}

	public static void setFNC_NM_DOT_ORCHS_SND_RVRSL_SSN_CMPLN(
			String fNC_NM_DOT_ORCHS_SND_RVRSL_SSN_CMPLN) {
		FNC_NM_DOT_ORCHS_SND_RVRSL_SSN_CMPLN = fNC_NM_DOT_ORCHS_SND_RVRSL_SSN_CMPLN;
	}

	public static String getFNC_NM_DOT_ORCHS_SND_VLDN() {
		return FNC_NM_DOT_ORCHS_SND_VLDN;
	}

	public static void setFNC_NM_DOT_ORCHS_SND_VLDN(String fNC_NM_DOT_ORCHS_SND_VLDN) {
		FNC_NM_DOT_ORCHS_SND_VLDN = fNC_NM_DOT_ORCHS_SND_VLDN;
	}

	public static String getFNC_NM_DOT_ORCHS_BP_VLDN() {
		return FNC_NM_DOT_ORCHS_BP_VLDN;
	}

	public static void setFNC_NM_DOT_ORCHS_BP_VLDN(String fNC_NM_DOT_ORCHS_BP_VLDN) {
		FNC_NM_DOT_ORCHS_BP_VLDN = fNC_NM_DOT_ORCHS_BP_VLDN;
	}

	public static String getFNC_NM_DOT_ORCHS_AMND_VLDN() {
		return FNC_NM_DOT_ORCHS_AMND_VLDN;
	}

	public static void setFNC_NM_DOT_ORCHS_AMND_VLDN(
			String fNC_NM_DOT_ORCHS_AMND_VLDN) {
		FNC_NM_DOT_ORCHS_AMND_VLDN = fNC_NM_DOT_ORCHS_AMND_VLDN;
	}

	public static String getFNC_NM_ORCHS_SND_VLDN() {
		return FNC_NM_ORCHS_SND_VLDN;
	}

	public static void setFNC_NM_ORCHS_SND_VLDN(String fNC_NM_ORCHS_SND_VLDN) {
		RVSServiceStartupBean.FNC_NM_ORCHS_SND_VLDN = fNC_NM_ORCHS_SND_VLDN;
	}

	public static String getFNC_NM_ORCHS_BP_VLDN() {
		return FNC_NM_ORCHS_BP_VLDN;
	}

	public static void setFNC_NM_ORCHS_BP_VLDN(String fNC_NM_ORCHS_BP_VLDN) {
		RVSServiceStartupBean.FNC_NM_ORCHS_BP_VLDN = fNC_NM_ORCHS_BP_VLDN;
	}

	public static String getFNC_NM_ORCHS_AMEND_VLDN() {
		return FNC_NM_ORCHS_AMEND_VLDN;
	}

	public static void setFNC_NM_ORCHS_AMEND_VLDN(String fNC_NM_ORCHS_AMEND_VLDN) {
		RVSServiceStartupBean.FNC_NM_ORCHS_AMEND_VLDN = fNC_NM_ORCHS_AMEND_VLDN;
	}

	public static String getFNC_NM_ORCHS_RCV_VLDN() {
		return FNC_NM_ORCHS_RCV_VLDN;
	}

	public static void setFNC_NM_ORCHS_RCV_VLDN(String fNC_NM_ORCHS_RCV_VLDN) {
		RVSServiceStartupBean.FNC_NM_ORCHS_RCV_VLDN = fNC_NM_ORCHS_RCV_VLDN;
	}

	public static String getFNC_NM_ORCHS_SESSION_CMPLN() {
		return FNC_NM_ORCHS_SESSION_CMPLN;
	}

	public static void setFNC_NM_ORCHS_SESSION_CMPLN(
			String fNC_NM_ORCHS_SESSION_CMPLN) {
		RVSServiceStartupBean.FNC_NM_ORCHS_SESSION_CMPLN = fNC_NM_ORCHS_SESSION_CMPLN;
	}

	public static String getFNC_NM_ORCHS_GET_FIELDS_FOR_PRDTS() {
		return FNC_NM_ORCHS_GET_FIELDS_FOR_PRDTS;
	}

	public static void setFNC_NM_ORCHS_GET_FIELDS_FOR_PRDTS(
			String fNC_NM_ORCHS_GET_FIELDS_FOR_PRDTS) {
		RVSServiceStartupBean.FNC_NM_ORCHS_GET_FIELDS_FOR_PRDTS = fNC_NM_ORCHS_GET_FIELDS_FOR_PRDTS;
	}

	public static String getFNC_NM_ORCHS_SND_RVRSL_VLDN() {
		return FNC_NM_ORCHS_SND_RVRSL_VLDN;
	}

	public static void setFNC_NM_ORCHS_SND_RVRSL_VLDN(
			String fNC_NM_ORCHS_SND_RVRSL_VLDN) {
		RVSServiceStartupBean.FNC_NM_ORCHS_SND_RVRSL_VLDN = fNC_NM_ORCHS_SND_RVRSL_VLDN;
	}

	public static String getFNC_NM_ORCHS_SND_RVRSL_SSN_CMPLN() {
		return FNC_NM_ORCHS_SND_RVRSL_SSN_CMPLN;
	}

	public static void setFNC_NM_ORCHS_SND_RVRSL_SSN_CMPLN(
			String fNC_NM_ORCHS_SND_RVRSL_SSN_CMPLN) {
		RVSServiceStartupBean.FNC_NM_ORCHS_SND_RVRSL_SSN_CMPLN = fNC_NM_ORCHS_SND_RVRSL_SSN_CMPLN;
	}
	/*
	public static String getFNC_NM_ORCHS_RCV_RVRSL() {
		return FNC_NM_ORCHS_RCV_RVRSL;
	}

	public static void setFNC_NM_ORCHS_RCV_RVRSL(String fNC_NM_ORCHS_RCV_RVRSL) {
		RVSServiceStartupBean.FNC_NM_ORCHS_RCV_RVRSL = fNC_NM_ORCHS_RCV_RVRSL;
	}

	public static String getFNC_ORCHS_RCV_RVRSL_SSN_COMPLN() {
		return FNC_ORCHS_RCV_RVRSL_SSN_COMPLN;
	}

	public static void setFNC_ORCHS_RCV_RVRSL_SSN_COMPLN(
			String fNC_ORCHS_RCV_RVRSL_SSN_COMPLN) {
		RVSServiceStartupBean.FNC_ORCHS_RCV_RVRSL_SSN_COMPLN = fNC_ORCHS_RCV_RVRSL_SSN_COMPLN;
	}

	public static String getFNC_NM_DOT_ORCHS_RCV_RVRSL() {
		return FNC_NM_DOT_ORCHS_RCV_RVRSL;
	}

	public static void setFNC_NM_DOT_ORCHS_RCV_RVRSL(
			String fNC_NM_DOT_ORCHS_RCV_RVRSL) {
		RVSServiceStartupBean.FNC_NM_DOT_ORCHS_RCV_RVRSL = fNC_NM_DOT_ORCHS_RCV_RVRSL;
	}

	public static String getFNC_NM_DOT_ORCHS_RCV_RVRSL_SSN_COMPLN() {
		return FNC_NM_DOT_ORCHS_RCV_RVRSL_SSN_COMPLN;
	}

	public static void setFNC_NM_DOT_ORCHS_RCV_RVRSL_SSN_COMPLN(
			String fNC_NM_DOT_ORCHS_RCV_RVRSL_SSN_COMPLN) {
		RVSServiceStartupBean.FNC_NM_DOT_ORCHS_RCV_RVRSL_SSN_COMPLN = fNC_NM_DOT_ORCHS_RCV_RVRSL_SSN_COMPLN;
	}

	*/

	public CountryCache getCountryCache() {
		return countryCache;
	}

	public void setCountryCache(CountryCache countryCache) {
		this.countryCache = countryCache;
	}

	public CountrySubDivCache getCountrySubDivCache() {
		return countrySubDivCache;
	}

	public void setCountrySubDivCache(CountrySubDivCache countrySubDivCache) {
		this.countrySubDivCache = countrySubDivCache;
	}

	public StateProvinceCache getStateProvinceCache() {
		return stateProvinceCache;
	}

	public void setStateProvinceCache(StateProvinceCache stateProvinceCache) {
		this.stateProvinceCache = stateProvinceCache;
	}

	public CurrencyInfoCache getCurrencyInfoCache() {
		return currencyInfoCache;
	}

	public void setCurrencyInfoCache(CurrencyInfoCache currencyInfoCache) {
		this.currencyInfoCache = currencyInfoCache;
	}

	public GetAllFieldsCache getGetAllFieldsCache() {
		return getAllFieldsCache;
	}

	public void setGetAllFieldsCache(GetAllFieldsCache getAllFieldsCache) {
		this.getAllFieldsCache = getAllFieldsCache;
	}

	public EnumerationDataCache getEnumerationDataCache() {
		return enumerationDataCache;
	}

	public void setEnumerationDataCache(EnumerationDataCache enumerationDataCache) {
		this.enumerationDataCache = enumerationDataCache;
	}

	public BadPhoneNumbersCache getBadPhoneNumbersCache() {
		return badPhoneNumbersCache;
	}

	public void setBadPhoneNumbersCache(BadPhoneNumbersCache badPhoneNumbersCache) {
		this.badPhoneNumbersCache = badPhoneNumbersCache;
	}

	public BadNameCache getBadNameCache() {
		return badNameCache;
	}

	public void setBadNameCache(BadNameCache badNameCache) {
		this.badNameCache = badNameCache;
	}

	public BadAddressCache getBadAddressCache() {
		return badAddressCache;
	}

	public void setBadAddressCache(BadAddressCache badAddressCache) {
		this.badAddressCache = badAddressCache;
	}

	public ESBODMDataMappingCache getEsbOdmDataMappingCache() {
		return esbOdmDataMappingCache;
	}

	public void setEsbOdmDataMappingCache(
			ESBODMDataMappingCache esbOdmDataMappingCache) {
		this.esbOdmDataMappingCache = esbOdmDataMappingCache;
	}

	public ESBMetaDataCache getEsbMetaDataCache() {
		return esbMetaDataCache;
	}

	public void setEsbMetaDataCache(ESBMetaDataCache esbMetaDataCache) {
		this.esbMetaDataCache = esbMetaDataCache;
	}

	public FactoryClassDataCache getFactoryClassDataCache() {
		return factoryClassDataCache;
	}

	public void setFactoryClassDataCache(FactoryClassDataCache factoryClassDataCache) {
		this.factoryClassDataCache = factoryClassDataCache;
	}

	List<String> countryCodeList = new ArrayList<String>();
	
	public List<String> getCountryCodeList() {
		return countryCodeList;
	}

	public void setCountryCodeList(List<String> countryCodeList) {
		this.countryCodeList = countryCodeList;
	}

	public RVSServiceStartupBean(){
		
	}
	
	public void startService() throws RequestValidationServiceException,Exception{
		BackupCacheImpl.getCacheObject();
		constructApplicationContextFactory();
		constructStartupCache();
		//createRulesetcache(); // ODM onload caching is not included Jan release
	}
	
	public void createRulesetcache() throws RequestValidationServiceException
	  {
	    try
	    {
	      IlrSessionFactory sessionFactory = new IlrPOJOSessionFactory();

	      sessionFactory.createManagementSession().loadUptodateRuleset(IlrPath.parsePath("/ValidationRuleApp/ValidationRuleset"));

	      
	    } catch (Exception e) {
	    	logger.error(" Error loading application context: "+e.getStackTrace());
	    	e.printStackTrace();
			RequestValidationServiceException excp= new RequestValidationServiceException("900","Error loading Ruleset");
			throw excp;
	    }
	  }

	private void constructStartupCache() throws RequestValidationServiceException, Exception {
		
		Map<String, CountryInfo> countryCodeMap=countryCache.getIsoCountryCodesForAllCountries();
		countryCodeList.addAll(countryCodeMap.keySet());
		logger.info("countrycache loaded");
		countrySubDivCache.getCountrySubDivList(countryCodeList);
		stateProvinceCache.getStateProvinceList(countryCodeList);
		currencyInfoCache.getCurrencyInfo();
		getAllFieldsCache.getAllFields(ReferenceConstants.API_DOMAIN_RCV);
		getAllFieldsCache.getAllFields(ReferenceConstants.API_DOMAIN_RCV_REVERSAL);
		getAllFieldsCache.getAllFields(ReferenceConstants.API_DOMAIN_SEND);
		getAllFieldsCache.getAllFields(ReferenceConstants.API_DOMAIN_SR);
		getAllFieldsCache.getAllFields(ReferenceConstants.API_DOMAIN_BILL_PYMNT);
		getAllFieldsCache.getAllFields(ReferenceConstants.API_DOMAIN_AMEND);
		getAllFieldsCache.getAllFields(ReferenceConstants.API_DOMAIN_TL);
		enumerationDataCache.getEnumeratedData();
		badNameCache.getBadConsumerNames();
		badAddressCache.getAddressBusinessEntitiesForCountries();
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_ORCHS_RCV_VLDN);
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_ORCHS_SESSION_CMPLN);
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_ORCHS_GET_FIELDS_FOR_PRDTS);
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_ORCHS_SND_RVRSL_VLDN);
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_ORCHS_SND_RVRSL_SSN_CMPLN);
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_ORCHS_AMEND_VLDN);
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_ORCHS_SND_VLDN);
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_ORCHS_BP_VLDN);
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_DOT_ORCHS_RCV_VLDN);
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_DOT_ORCHS_SESSION_CMPLN);
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_DOT_ORCHS_GET_FIELDS_FOR_PRDTS);
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_DOT_ORCHS_SND_RVRSL_VLDN);
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_DOT_ORCHS_SND_RVRSL_SSN_CMPLN);
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_DOT_ORCHS_SND_VLDN);
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_DOT_ORCHS_BP_VLDN);
		esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_DOT_ORCHS_AMND_VLDN);
		//esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_ORCHS_RCV_RVRSL);
		//esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_ORCHS_RCV_RVRSL_SSN_COMPLN);
		//esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_DOT_ORCHS_RCV_RVRSL);
		//esbOdmDataMappingCache.getESBOdmAttrbutesMapping(FNC_NM_DOT_ORCHS_RCV_RVRSL_SSN_COMPLN);
		factoryClassDataCache.getODMGroupFactoryClass();
	
		
		
	}

	private void constructApplicationContextFactory() throws RequestValidationServiceException {
		
		try{
		ApplicationContextFactory.setContextResources(CONTEXT_FILES);
		logger.info("Context loaded successfully");
		}catch(Exception e){
			logger.error(" Error loading application context: "+e.getStackTrace());
			RequestValidationServiceException excp= new RequestValidationServiceException("900","Error loading application context");
			throw excp;
		}
		
		
	}
	
	public void stopService(){
		
		ApplicationContext context= ApplicationContextProvider.getApplicationContext();
		
		//ClassPathXmlApplicationContext context= (ClassPathXmlApplicationContext)ApplicationContextFactory.getApplicationContext();
		context =null;
		logger.info("context destroyed");
	}
	
	
	
	
	
	
	

}
