package com.moneygram.dvr.service.utility;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.dvr.service.constants.DVRConstants;

public class AttributeXmlMapper {
	static AttributeXmlMapper axm = new AttributeXmlMapper();
	private static Logger logger = LogFactory.getInstance().getLogger(AttributeXmlMapper.class);
	static Map<String,Map<String,String>> consumerMap;
	
	public static Map<String,Map<String,String>> getExternalData(String attributeXml){
		if(consumerMap==null){
			consumerMap = externalData(attributeXml);
			AttributeXmlMapper.setConsumerMap(consumerMap);
		}
		return consumerMap;
	}
	public static Map<String,Map<String,String>> externalData(String attributeXml){
		//Testing with external XML
		
		try {

			 consumerMap = new HashMap<String,Map<String,String>>();

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse (new File(attributeXml));
			logger.info("Root element of the doc is " + doc.getDocumentElement().getNodeName());
			NodeList listOfAttributes = null;
		//	if(DVRConstants.SENDER.equalsIgnoreCase(validationId)){
			Map<String, String> esbDvrSendMap = new HashMap<String,String>();
			Map<String, String> esbDvrReceiveMap = new HashMap<String,String>();
			Map<String, String> esbDvrTpSendMap = new HashMap<String,String>();

				listOfAttributes = doc.getElementsByTagName("sender");
				if(listOfAttributes!=null){
					int totalSenders = listOfAttributes.getLength();
					logger.info("Total no of Senders : " + totalSenders);
					esbDvrSendMap = fetchDetails(listOfAttributes,esbDvrSendMap);
					 consumerMap.put(DVRConstants.SENDER, esbDvrSendMap);
				}
				
		//	}
			//else if(DVRConstants.RECEIVER.equalsIgnoreCase(validationId)){


				listOfAttributes = doc.getElementsByTagName("receiver");
				if(listOfAttributes!=null){
					int totalReceivers = listOfAttributes.getLength();
					logger.info("Total no of Receivers : " + totalReceivers);
					esbDvrReceiveMap = fetchDetails(listOfAttributes,esbDvrReceiveMap);
					consumerMap.put(DVRConstants.RECEIVER, esbDvrReceiveMap);
				}
			//}
			//else if(DVRConstants.THIRDPARTY.equalsIgnoreCase(validationId)){


				listOfAttributes = doc.getElementsByTagName("thirdParty");
				if(listOfAttributes!=null){
					int totalTPSenders = listOfAttributes.getLength();
					logger.info("Total no of ThirdParty senders : " + totalTPSenders);
					esbDvrTpSendMap = fetchDetails(listOfAttributes,esbDvrTpSendMap);
					consumerMap.put(DVRConstants.THIRDPARTY, esbDvrTpSendMap);
				}
			//}

			/*if(listOfAttributes!=null){
				esbDvrMap = fetchDetails(listOfAttributes,esbDvrMap);
			}*/

		}catch (SAXParseException err) {
			System.out.println ("** Parsing error" + ", line " + err.getLineNumber () + ", uri " + err.getSystemId ());
			System.out.println(" " + err.getMessage ());

			}catch (SAXException e) {
			Exception x = e.getException ();
			((x == null) ? e : x).printStackTrace ();

			}catch (Throwable t) {
			t.printStackTrace ();
			}
			//return esbDvrMap;
			return consumerMap;
	}
	
	public static Map<String,String> fetchDetails(NodeList listOfAttributes,Map<String,String> esbDvrMap){
		for(int s=0; s<listOfAttributes.getLength() ; s++){

			String esbAttribute=null;
			String dvrAttribute=null;
			Node firstPersonNode = listOfAttributes.item(s);
			if(firstPersonNode.getNodeType() == Node.ELEMENT_NODE){


			Element firstPersonElement = (Element)firstPersonNode; 

			//-------
			NodeList firstNameList = firstPersonElement.getElementsByTagName("esbAttribute");
			Element firstNameElement = (Element)firstNameList.item(0);

			NodeList textFNList = firstNameElement.getChildNodes();
			//logger.info("ESB Attribute : " + ((Node)textFNList.item(0)).getNodeValue().trim());
			esbAttribute=((Node)textFNList.item(0)).getNodeValue().trim();
			//------- 
			NodeList lastNameList = firstPersonElement.getElementsByTagName("dvrAttribute");
			Element lastNameElement = (Element)lastNameList.item(0);

			NodeList textLNList = lastNameElement.getChildNodes();
			//logger.info("DVR Attribute : " + ((Node)textLNList.item(0)).getNodeValue().trim());
			dvrAttribute=((Node)textLNList.item(0)).getNodeValue().trim();
			//----
			esbDvrMap.put(dvrAttribute,esbAttribute);
			//------

			}//end of if clause
			

			}
		
		return esbDvrMap;
	}
	public static Map<String, Map<String, String>> getConsumerMap() {
		return consumerMap;
	}
	public static void setConsumerMap(Map<String, Map<String, String>> consumerMap) {
		AttributeXmlMapper.consumerMap = consumerMap;
	}
}
