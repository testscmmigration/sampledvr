package com.moneygram.dvc.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class RuleReader {
	public String read(String fileName) throws IOException{
		BufferedReader reader;
    	StringBuffer xmlBuffer = new StringBuffer();
		InputStream in = getClass().getResourceAsStream(fileName) ;
		reader = new BufferedReader(new InputStreamReader(in));
		
		while(reader.ready()) {
			xmlBuffer.append(reader.readLine()+ "\n");
		}
		
		return xmlBuffer.toString();
	}
}
