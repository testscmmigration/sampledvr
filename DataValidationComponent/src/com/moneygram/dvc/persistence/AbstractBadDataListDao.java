package com.moneygram.dvc.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.object.StoredProcedure;

import com.moneygram.rule.common.datalist.RuleDataListFacade;

/**
 * AbstractBadDataListDao provides the common behaviors of all the Bad Data List
 * implementations.
 * <p>
 * It assumes the following:
 * <li> The data is retrieved via a stored procedure call.
 * <li> The stored procedure as 2 input and 2 output parameters.
 * <li> The first input parameter is the Business Area Code.
 * <li> The second input parameter is the Call Type Code.
 * <li> The first output parameter is the Process Log Id.
 * <li> The second output parameter is the Cursor.
 * <p>
 * It is left to the implementing class to:
 * <li> Define the stored procedure name.
 * <li> Define the input and output parameter names.
 * <li> Implement a {@link RowMapper}.
 */
public abstract class AbstractBadDataListDao extends JdbcDaoSupport  implements RuleDataListFacade {

	public AbstractBadDataListDao() {
		super();
	}

	@Override
	public List<?> find() throws Exception {
		StoredProcedure proc = new BadDataListStoredProcedure(getJdbcTemplate(), getStoredProcedureName());
		
		Map<String, Object> inputParams = generateInputMap();
		
		Map<String, Object> results = proc.execute(inputParams);
		
		return (List<?>) results.get(getCursorOutputName());
	}

	public abstract RowMapper<?> getRowMapper();
	public abstract String getStoredProcedureName();
	public abstract String getBusinessAreaCodeInputName();
	public abstract String getCallTypeCodeInputName();
	public abstract String getProcessLogIdOutputName();
	public abstract String getCursorOutputName();

	
	protected Map<String, Object> generateInputMap() {
		Map<String, Object> inputParams = new HashMap<String, Object>();
		inputParams.put(getBusinessAreaCodeInputName(), businessAreaCode);
		inputParams.put(getCallTypeCodeInputName(), callTypeCode);
		return inputParams;
	}

	public void setBusinessAreaCode(String businessAreaCode) {
		this.businessAreaCode = businessAreaCode;
	}

	public void setCallTypeCode(String callTypeCode) {
		this.callTypeCode = callTypeCode;
	}

	// Spring DI properties.
	protected String businessAreaCode;
	protected String callTypeCode;

	/**
	 * BadDataListStoredProcedure provides processing to call the stored
	 * procedure to retrieve the bad data list for configured business area
	 * code.
	 * <p>
	 * It relies on the abstract methods to retrieve the input and output field
	 * names. And to retrieve the proper {@link RowMapper} instance.
	 * 
	 */
	protected class BadDataListStoredProcedure extends StoredProcedure {
		
		public BadDataListStoredProcedure(JdbcTemplate jdbcTemplate, String storedProcName) {
			super(jdbcTemplate, storedProcName);
			
			declareParameter(new SqlParameter(getBusinessAreaCodeInputName(), OracleTypes.VARCHAR));
			declareParameter(new SqlParameter(getCallTypeCodeInputName(), OracleTypes.VARCHAR));
			declareParameter(new SqlOutParameter(getProcessLogIdOutputName(), OracleTypes.NUMBER));
			declareParameter(new SqlOutParameter(getCursorOutputName(), OracleTypes.CURSOR, getRowMapper()));
			
			compile();
		}
	}
}