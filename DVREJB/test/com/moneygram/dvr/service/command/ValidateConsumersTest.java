package com.moneygram.dvr.service.command;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.moneygram.service.dvr_v1.ConsumerValidationRequest;
import com.moneygram.service.framework.command.CommandException;
import com.sun.xml.internal.ws.util.Pool.Marshaller;

public class ValidateConsumersTest {
	
	@Autowired
	ValidateConsumersCommand command;
	
	@Before
	public void setUp()
	{
		System.out.println("Inside SetUp................");
		ApplicationContext context= new ClassPathXmlApplicationContext("DVRapplicationContext-test.xml");
		command=(ValidateConsumersCommand) context.getBean("validateConsumersCommand");
		System.out.println(command);
		
	}
	
	@Test
	public void testProcess() throws CommandException, IOException, JAXBException
	{
		
		String request = readFile("test/DVR_Rcv_request.xml");
		JAXBContext jaxbContext= JAXBContext.newInstance("com.moneygram.service.dvr_v1");
		Unmarshaller unmarshal= jaxbContext.createUnmarshaller();
		ConsumerValidationRequest consumerValidationRequest=(ConsumerValidationRequest) unmarshal.unmarshal(new StringReader(request));
		
		command.process(consumerValidationRequest);
		
	}
	
	
	private static String readFile(String path) throws IOException {
		FileInputStream stream = new FileInputStream(new File(path));
		try {
			FileChannel fc = stream.getChannel();
			MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0,
					fc.size());
			/* Instead of using default, pass in a decoder. */
			return Charset.defaultCharset().decode(bb).toString();
		} finally {
			stream.close();
		}
	}

}
